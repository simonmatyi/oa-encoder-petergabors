cmake_minimum_required(VERSION 3.7)

include(Qt)
include(Catch)

set(TEST_SOURCES
        tst_encoder.cpp
        tst_codec.cpp
   )

set(PRODUCTIVE_SOURCES
        ${CMAKE_SOURCE_DIR}/src/encoder.cpp
        ${CMAKE_SOURCE_DIR}/src/codec.cpp
   )

add_executable(unit unit.cpp ${TEST_SOURCES} ${PRODUCTIVE_SOURCES})
target_link_libraries(unit Qt5::Core)
