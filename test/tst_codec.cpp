#include <catch.hpp>

#include "codec.h"

#include <QDebug>

class mock_Codec : public Codec
{
public:
    const QMap<QChar, QString> & mock_dictonary() const
    {
        return dictionary();
    }
};

TEST_CASE("Codec loadDictionary")
{
    mock_Codec codec;
    QString input;
    QJsonDocument inputJson;
    QJsonParseError parseError;

    SECTION("Empty json")
    {
        input = QStringLiteral(R"({})");
        inputJson = QJsonDocument::fromJson(input.toUtf8(), &parseError);
        REQUIRE(parseError.error == QJsonParseError::NoError);

        REQUIRE(codec.loadDictionary(inputJson) == true);
        REQUIRE(codec.mock_dictonary().size() == 0);
    }

    SECTION("Single element")
    {
        input = QStringLiteral(R"({ "a": "a" })");
        inputJson = QJsonDocument::fromJson(input.toUtf8(), &parseError);
        REQUIRE(parseError.error == QJsonParseError::NoError);

        REQUIRE(codec.loadDictionary(inputJson) == true);
        REQUIRE(codec.mock_dictonary().size() == 1);
    }

    SECTION("Empty key")
    {
        input = QStringLiteral(R"({ "": "a" })");
        inputJson = QJsonDocument::fromJson(input.toUtf8(), &parseError);
        REQUIRE(parseError.error == QJsonParseError::NoError);

        REQUIRE(codec.loadDictionary(inputJson) == false);
        REQUIRE(codec.mock_dictonary().size() == 0);
    }

    SECTION("Two char long key")
    {
        input = QStringLiteral(R"({ "aa": "a" })");
        inputJson = QJsonDocument::fromJson(input.toUtf8(), &parseError);
        REQUIRE(parseError.error == QJsonParseError::NoError);

        REQUIRE(codec.loadDictionary(inputJson) == false);
        REQUIRE(codec.mock_dictonary().size() == 0);
    }

    SECTION("Empty value")
    {
        input = QStringLiteral(R"({ "a": "" })");
        inputJson = QJsonDocument::fromJson(input.toUtf8(), &parseError);
        REQUIRE(parseError.error == QJsonParseError::NoError);

        REQUIRE(codec.loadDictionary(inputJson) == false);
        REQUIRE(codec.mock_dictonary().size() == 0);
    }

    SECTION("Invalid (empty) QJsonDocument")
    {
        input = QStringLiteral(R"(a)");
        inputJson = QJsonDocument::fromJson(input.toUtf8(), &parseError);
        REQUIRE(parseError.error != QJsonParseError::NoError);

        REQUIRE(codec.loadDictionary(inputJson) == true);
        REQUIRE(codec.mock_dictonary().size() == 0);
    }

    SECTION("Non-string value")
    {
        input = QStringLiteral(R"({ "a": true })");
        inputJson = QJsonDocument::fromJson(input.toUtf8(), &parseError);
        REQUIRE(parseError.error == QJsonParseError::NoError);

        REQUIRE(codec.loadDictionary(inputJson) == false);
        REQUIRE(codec.mock_dictonary().size() == 0);
    }

    SECTION("Two elements")
    {
        input = QStringLiteral(R"({ "a": "a", "b": "b" })");
        inputJson = QJsonDocument::fromJson(input.toUtf8(), &parseError);
        REQUIRE(parseError.error == QJsonParseError::NoError);

        REQUIRE(codec.loadDictionary(inputJson) == true);
        REQUIRE(codec.mock_dictonary().size() == 2);
    }

    SECTION("Code string with length 11")
    {
        input = QStringLiteral(R"({ "a": "a", "b": "b", "c": "1234567890a" })");
        inputJson = QJsonDocument::fromJson(input.toUtf8(), &parseError);
        REQUIRE(parseError.error == QJsonParseError::NoError);

        REQUIRE(codec.loadDictionary(inputJson) == false);
        REQUIRE(codec.mock_dictonary().size() == 0);
    }
}

TEST_CASE("Codec encode")
{
    mock_Codec codec;
    QString dictionaryInput;
    bool encodeError;
    QJsonDocument dictionary;
    QJsonParseError parseError;

    dictionaryInput = QStringLiteral(R"({ "a": "b", "c": "d", "E": "f", "g": "hi" })");
    const int dictionaryInputElementCount = 4;
    dictionary = QJsonDocument::fromJson(dictionaryInput.toUtf8(), &parseError);
    REQUIRE(parseError.error == QJsonParseError::NoError);
    REQUIRE(codec.loadDictionary(dictionary) == true);

    SECTION("Empty string")
    {
        encodeError = true;

        REQUIRE(codec.encode("", &encodeError) == "");
        REQUIRE(encodeError == false);
    }

    SECTION("Single character")
    {
        encodeError = true;

        REQUIRE(codec.encode("a", &encodeError) == "b");
        REQUIRE(encodeError == false);
    }

    SECTION("Upper case character")
    {
        encodeError = true;

        REQUIRE(codec.encode("A", &encodeError) == "b");
        REQUIRE(encodeError == false);
    }

    SECTION("Upper case character in the dictionary")
    {
        encodeError = true;

        REQUIRE(codec.encode("e", &encodeError) == "f");
        REQUIRE(encodeError == false);
    }

    SECTION("Upper case character in the dictionary and the input")
    {
        encodeError = true;

        REQUIRE(codec.encode("E", &encodeError) == "f");
        REQUIRE(encodeError == false);
    }

    SECTION("Invalid characters")
    {
        encodeError = false;

        REQUIRE(codec.encode("z", &encodeError) == "");
        REQUIRE(encodeError == true);
        /* Check against growing dictionary */
        REQUIRE(codec.mock_dictonary().size() == dictionaryInputElementCount);
    }

    SECTION("Two characters")
    {
        encodeError = true;

        REQUIRE(codec.encode("aa", &encodeError) == "bb");
        REQUIRE(encodeError == false);
    }

    SECTION("Input yielding more than one character of output")
    {
        encodeError = true;

        REQUIRE(codec.encode("g", &encodeError) == "hi");
        REQUIRE(encodeError == false);
    }
}

TEST_CASE("Codec decode")
{
    mock_Codec codec;
    QString dictionaryInput;
    bool decodeError;
    QJsonDocument dictionary;
    QJsonParseError parseError;

    dictionaryInput = QStringLiteral(R"({ "a": "b", "c": "d", "E": "f", "g": "hi" })");
    const int dictionaryInputElementCount = 4;
    dictionary = QJsonDocument::fromJson(dictionaryInput.toUtf8(), &parseError);
    REQUIRE(parseError.error == QJsonParseError::NoError);
    REQUIRE(codec.loadDictionary(dictionary) == true);

    SECTION("Empty string")
    {
        decodeError = true;

        REQUIRE(codec.decode("", &decodeError) == "");
        REQUIRE(decodeError == false);
    }

    SECTION("Single code string")
    {
        decodeError = true;

        REQUIRE(codec.decode("b", &decodeError) == "a");
        REQUIRE(decodeError == false);
    }

    SECTION("Invalid code string")
    {
        decodeError = false;

        REQUIRE(codec.decode("z", &decodeError) == "");
        REQUIRE(decodeError == true);
    }

    SECTION("Coded string corresponding to two letters")
    {
        decodeError = false;

        REQUIRE(codec.decode("hid", &decodeError) == "gc");
        REQUIRE(decodeError == false);
    }

    SECTION("Long coded string")
    {
        decodeError = false;

        REQUIRE(codec.decode("hidbbhiff", &decodeError) == "gcaagee");
        REQUIRE(decodeError == false);
    }

    SECTION("Long coded string with one invalid coded substring")
    {
        decodeError = false;

        REQUIRE(codec.decode("hidzdd", &decodeError) == "");
        REQUIRE(decodeError == true);
    }
}
