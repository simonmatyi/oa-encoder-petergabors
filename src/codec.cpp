#include "codec.h"

#include <QDebug>

Codec::Codec()
{

}

bool Codec::loadDictionary(const QJsonDocument & json)
{
    bool parseOk = true;
    QJsonObject jsonObj = json.object();

    QMap<QChar, QString> tmpDict;
    QMap<QString, QChar> tmpReverseDict;

    for(auto element = jsonObj.constBegin(); parseOk && (element != jsonObj.constEnd()); element++)
    {
        if(  (element.key().size() == 1)
          && (element.value().toString().size() > 0)
          && (element.value().toString().size() <= 10)
          )
        {
            tmpDict.insert(element.key().at(0).toLower(), element.value().toString(""));
            tmpReverseDict.insert(element.value().toString(""), element.key().at(0).toLower());
        }
        else {
            parseOk = false;
        }
    }

    if(parseOk)
    {
        dict = tmpDict;
        reverseDict = tmpReverseDict;
    }

    return parseOk;
}

QString Codec::encode(const QString & input, bool * encodeError) const
{
    QString output;
    if(encodeError != nullptr)
    {
        (*encodeError) = false;
    }

    for(QChar ch : input)
    {
        if(dict.keys().contains(ch.toLower()))
        {
            output.push_back(dict[ch.toLower()]);
        }
        else
        {
            if(encodeError != nullptr)
            {
                (*encodeError) = true;
            }
            output = "";
        }
    }

    return output;
}

QString Codec::decode(const QString & input, bool * decodeError) const
{
    QString output="";
    QString remainingInput = input;

    if(decodeError != nullptr)
    {
        (*decodeError) = false;
    }

    bool match = true;

    while(match && (!remainingInput.isEmpty()))
    {
        match = false;

        for(QString currentCodeStr : reverseDict.keys())
        {
            if(remainingInput.startsWith(currentCodeStr, Qt::CaseInsensitive))
            {

                remainingInput.remove(0, currentCodeStr.size());
                output += reverseDict[currentCodeStr];
                match = true;
                break;
            }
        }

        if(!match && (decodeError != nullptr))
        {
            output = "";
            (*decodeError) = true;
        }
    }

    return output;
}

const QMap<QChar, QString> & Codec::dictionary() const
{
    return dict;
}
