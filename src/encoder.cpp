#include "encoder.h"

#include <iostream>
#include <QFile>
#include <QDebug>

Encoder::Encoder()
{

}

void Encoder::run()
{
    loadJson();

    std::string userInput;

    while(userInput != "EXIT")
    {
        std::getline(std::cin, userInput);

        QString qUserInput = QString::fromStdString(userInput);

        if(qUserInput.startsWith("ENCODE_FILE") || qUserInput.startsWith("DECODE_FILE"))
        {
            QStringList parameters = qUserInput.split(' ', QString::SkipEmptyParts);
            if(parameters.size() == 3)
            {
                if(qUserInput.startsWith("EN"))
                {
                    encodeFile(parameters[1], parameters[2]);
                }
                else
                {
                    decodeFile(parameters[1], parameters[2]);
                }
            }
        }
        else if(qUserInput.startsWith("ENCODE"))
        {
            encodeString(qUserInput.remove(0, strlen("ENCODE")));
        }
        else if(qUserInput.startsWith("DECODE"))
        {
            decodeString(qUserInput.remove(0, strlen("DECODE")));
        }
    }

    std::cout << "KTHXBYE" << std::endl;
}

bool Encoder::loadFile(QString path, QByteArray & outFileContents)
{
    if(QFile::exists(path))
    {
        QFile inputFile(path);

        if(inputFile.open(QFile::ReadOnly))
        {
            outFileContents = inputFile.readAll();
            inputFile.close();
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }

    return true;
}

void Encoder::loadJson()
{
    bool success = false;

    while(!success)
    {
        std::string jsonPath;

        std::cin >> jsonPath;

        QByteArray fileContents;

        if(loadFile(QString::fromStdString(jsonPath), fileContents))
        {
            QJsonParseError parseError;
            QJsonDocument json = QJsonDocument::fromJson(fileContents, &parseError);

            if((parseError.error == QJsonParseError::NoError) && codec.loadDictionary(json))
            {
                success = true;
            }
        }

        if(!success)
        {
            std::cout<<"ERROR: Invalid coding table"<<std::endl;
        }
    }
}

QString Encoder::encodeDecode(bool encode, QString input, QString errorMsg, bool * error)
{
    bool err;
    QString output;

    if(encode)
    {
        output = codec.encode(input, &err);
    }
    else
    {
        output = codec.decode(input, &err);
    }

    if(err)
    {
        output = errorMsg;
    }

    if(error != nullptr)
    {
        (*error) = err;
    }

    return output;
}

void Encoder::encodeString(QString input)
{
    std::cout << encodeDecode(true, input, "INVALID INPUT").toStdString() << std::endl;
}

void Encoder::decodeString(QString input)
{
    std::cout << encodeDecode(false, input, "INVALID INPUT").toStdString() << std::endl;
}

bool Encoder::writeToFile(QString path, QString content)
{
    QFile outputFile(path);

    if(outputFile.open(QFile::WriteOnly))
    {
        QByteArray data = content.toUtf8();
        qint64 bytesWritten = outputFile.write(data);
        outputFile.close();

        return bytesWritten == data.size();
    }
    else {
        return false;
    }
}

void Encoder::encodeDecodeFile(bool encode, QString inputPath, QString outputPath)
{
    QByteArray input;

    if(loadFile(inputPath, input))
    {
        bool error;

        QString output = encodeDecode(encode, input, "", &error);

        if(!error)
        {
            writeToFile(outputPath, output);
        }
    }
    else
    {
        std::cout<<"INVALID INPUT FILE"<<std::endl;
    }
}

void Encoder::encodeFile(QString inputPath, QString outputPath)
{
    encodeDecodeFile(true, inputPath, outputPath);
}

void Encoder::decodeFile(QString inputPath, QString outputPath)
{
    encodeDecodeFile(false, inputPath, outputPath);
}
