#ifndef ENCODER_H
#define ENCODER_H

#include "codec.h"
#include <QString>

class Encoder
{
public:
    Encoder();

    void run();

    bool loadFile(QString path, QByteArray & outFileContents);
    void loadJson();
    QString encodeDecode(bool encode, QString input, QString errorMsg, bool * error = nullptr);
    void encodeString(QString input);
    void decodeString(QString input);
    bool writeToFile(QString path, QString content);
    void encodeDecodeFile(bool encode, QString inputPath, QString outputPath);
    void encodeFile(QString inputPath, QString outputPath);
    void decodeFile(QString inputPath, QString outputPath);

private:
    Codec codec;
};

#endif // ENCODER_H
