#ifndef CODEC_H
#define CODEC_H

#include <QString>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMap>

class Codec
{
public:
    Codec();

    bool loadDictionary(const QJsonDocument & json);
    QString encode(const QString & input, bool * encodeError = nullptr) const;
    QString decode(const QString & input, bool * decodeError = nullptr) const;

protected:
    const QMap<QChar, QString> & dictionary() const;

private:
    QMap<QChar, QString> dict;
    QMap<QString, QChar> reverseDict;
};

#endif // CODEC_H
