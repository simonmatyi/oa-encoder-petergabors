#include <iostream>

#include "encoder.h"

int main(int argc, char * argv[])
{
    Encoder encoder;

    encoder.run();

    return 0;
}

