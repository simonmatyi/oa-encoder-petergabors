#!/usr/bin/env bash

mkdir build
cd build || exit
cmake ../src

#check the presence of 'nproc'
command -v nproc
if [ $? -eq 0 ]
then
	make -j $(nproc)
else
	make
fi

cp /src/run.sh /
rm -Rf /src/

