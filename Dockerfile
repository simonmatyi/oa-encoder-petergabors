FROM ubuntu:18.04

# Install build environment
RUN apt-get update && apt-get -y install build-essential cmake qt5-default catch

# Copy source files
COPY ./ /src/

# Build
WORKDIR /
RUN ./src/build.sh

# Run the server
ENTRYPOINT ["./run.sh"]

